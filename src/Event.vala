
using Bg;
namespace Bg{

	public class Event{
		public Event(){
			LOOP_INT = LOOP_INT + 1;
			if(m_event == null)
				m_event = new Event.with_null();
			if(eloop == null)
				eloop = new EventLooping(m_event);
		}
		public Event.with_null(){

		}

		~Event(){
			LOOP_INT = LOOP_INT - 1;
			if(LOOP_INT == 0){
				eloop.kill();
			}
		}
		public int get_loop_id(){
			return LOOP_INT;
		}
		private static int LOOP_INT = 0;
		private static EventLooping eloop = null;

		public signal void onClick(int x, int y);
		public signal void onRelease(int x, int y);
		public signal void onClickDown(int x, int y);
		public signal void onClose();
		public signal void onKeyUp(string touche);
		public signal void onKeyDown(string touche);


		public static Event get_loop_event(){
			return m_event;
		}
		private static Event m_event = null;
	}


	public class EventLooping{
		public EventLooping(Event *e){
			print("Activation de la loop d'event\n");
			m_event = e;
			m_thread = new Thread<void>("loopevent", this.run_loop);
		}
		public void kill(){
			print("Fin de boucle event\n");
			thread_run = false;
			m_thread.join();
			print("attente du thread terminer\n");
		}

		private void run_loop(){

			SDL.Event e;
			while(m_event.get_loop_id() != 0)
			{
				SDL.Event.poll(out e);
				if(e.type == SDL.EventType.QUIT)
					m_event.onClose();
                
                if(e.type == SDL.EventType.KEYUP)
                {
                    m_event.onKeyUp(e.key.keysym.scancode.get_name());
                }
                if(e.type == SDL.EventType.KEYDOWN)
                {
                    m_event.onKeyDown(e.key.keysym.scancode.get_name());
                }
				if(e.type == SDL.EventType.MOUSEBUTTONUP)
					m_event.onClick(e.motion.x, e.motion.y);
				if(e.type == SDL.EventType.MOUSEBUTTONDOWN)
					m_event.onRelease(e.motion.x, e.motion.y);
				if(e.type == SDL.EventType.MOUSEBUTTONDOWN)
					m_event.onClickDown(e.motion.x, e.motion.y);
			}
			print("/!\\  Fin de la loop  ON A REUSSIS\n");
		}
		private static bool thread_run = true;
		private static Thread<void> m_thread;
		private static Event m_event = null;
	}
	public enum Direction{
		LEFT,RIGHT;
	}
}
