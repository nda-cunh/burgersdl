using Bg;
namespace Bg{

	public class Drawable{
		public Drawable(){

		}

		/*################################################*/
		/*               Fonction Colision                */
		/*################################################*/

		public bool collision_point(Vector2i point){
			if(m_whats_is == CIRCLE){
				double d2 = (point.x - position.x)*(point.x - position.x) + (point.y - position.y)*(point.y - position.y);
				if (d2 > m_radius*m_radius)
					return false;
				else
					return true;
			}
			if(m_whats_is == SPRITE || m_whats_is == RECTANGLE){
				int width = m_rect.w;
				int height = m_rect.h;

				if (point.x >= position.x && point.x < position.x + width && point.y >= position.y && point.y < position.y + height)
					return true;
				else
					return false;
			}
			return false;
		}

		public bool collision_rect(Sprite sprite)
		{
			var box1 = position;
			var box2 = sprite.position;
			var box1w = m_texture.w;
			var box2w = sprite.m_texture.w;
			var box1h = m_texture.h;
			var box2h = sprite.m_texture.h;

			if((box2.x >= box1.x + box1w) || (box2.x + box2w <= box1.x) || (box2.y >= box1.y + box1h) || (box2.y + box2h <= box1.y))
				return false;
			else
				return true;
		}

		/*public bool collision_circle(Circle cercle){
		  double d2;
		  if(m_whats_is == CIRCLE)
		  {
		  d2 = (int)(m_pos.x -cercle.get_position().x)*(m_pos.x-cercle.get_position().x) + (m_pos.y-cercle.get_position().y)*(m_pos.y-cercle.get_position().y);
		  if (d2 > (m_radius + cercle.get_radius())*(m_radius + cercle.get_radius()))
		  return false;
		  else
		  return true;
		  }
		  else{
		  print("Nexiste pas encore pour ce dessin");
		  return false;
		  }
		  }*/

		/*################################################*/
		/*                Fonction Special                */
		/*################################################*/

        public void move(int x, int y)
        {
            m_position.x = m_position.x + x;
            m_position.y = m_position.y + y;
        }
		public void rotate(int degree){
			m_angle = m_angle + degree;
		}
		public void set_rotate(int degree){
			m_angle = degree;
		}

        public void set_Position(int x, int y)
        {
            m_position.x = x;
            m_position.y = y;
        }

		public void scale(double size){
			m_size.x += size;
			m_size.y += size;
		}

		public void set_scale(double size){
			m_size.x = size;
			m_size.y = size;
		}

		/*################################################*/
		/*                Variable public                 */
		/*################################################*/

		public int angle{
			get {return m_angle;}
			public set{m_angle = value;}
		}

        public Vector2f position{
			get {return m_position;}
			public set{m_position = value;}
		}

		public Bg.Vector2f size{
			get {return m_size;}
			public set{m_size = value;}
		}

		public int radius{
			get {return m_radius;}
			public set{m_radius = value;}
		}

		public Color outline_color{
			get {return m_outline_color;}
			private set{;}
		}

		public Color color{
			get {return m_color;}
			private set{;}
		}




		public int outline_radius{
			get {return m_outline_radius;}
			private set{;}
		}

		public Rect rect{
			get {return m_rect;}
			public set{m_rect = value;}
		}

		public IS_DRAWABLE whats_is{
			get {return m_whats_is;}
			private set{;}
		}

		public void set_render(SDL.Video.Renderer render, bool force=false){
			if(m_texture_sdl == null){
				m_texture_sdl = SDL.Video.Texture.create_from_surface (render, m_texture.get_surface());
				return;
			}
			if(force == true)
				m_texture_sdl = SDL.Video.Texture.create_from_surface (render, m_texture.get_surface());
		}

		public Texture get_texture(){
			return m_texture;
		}

		public SDL.Video.Texture *get_texture_sdl(){
			return m_texture_sdl;
		}

		/*################################################*/
		/*                Variable Private                */
		/*################################################*/

		protected int m_angle = 0;
		protected Color m_outline_color;
		protected int m_radius;
		protected Color m_color;
		protected IS_DRAWABLE m_whats_is;
		protected Vector2f m_size;
		protected Vector2f m_position;
		protected int m_outline_radius = 0;
		protected Rect m_rect;
		protected SDL.Video.Texture m_texture_sdl;
		protected Texture m_texture;
	}

	public enum IS_DRAWABLE{
		RECTANGLE, SPRITE, CIRCLE, SHAPE
	}
}
