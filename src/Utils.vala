using Bg;
namespace Bg{

	public struct Color{
		uchar r;
		uchar g;
		uchar b;
		uchar a;
		public static Color(int red = 255,int green = 255, int blue = 255, int alpha = 255){
		    r = (uchar)red;
		    g = (uchar)green;
		    b = (uchar)blue;
		    a = (uchar)alpha;
        }
	}

	public struct Rect{		
	    int x;
		int y;
		int w;
		int h;
	}

	public struct Vector2i{
		int x;
		int y;
		public static Vector2i(int xpos, int ypos){
		    x = xpos;
		    y = ypos;
		}
	}

	public struct Vector2f{
		double x;
		double y;
		public static Vector2f(double xpos, double ypos){
		    x = xpos;
		    y = ypos;
		}
	}
}
